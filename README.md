This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Redux example from https://github.com/reactjs/redux/blob/master/examples/counter-vanilla/index.html has been added to the app.

This app is meant to be a boilerplate app for react-redux code and use case analysis.
