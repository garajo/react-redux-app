/*
  Based on create-react-app
  and
  redux Counter Vanilla - https://github.com/reactjs/redux/blob/master/examples/counter-vanilla/index.html
*/

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {connect} from 'react-redux';

class Counter extends Component {
  render() {
    const { count, update } = this.props
    return (
      <div>
        <p>
          Clicked: <span id="value">{count}</span> times
          <button id="increment" onClick={() => {  update('INCREMENT')}}>+</button>
          <button id="decrement" onClick={() => { update('DECREMENT')} }>-</button>
          <button id="incrementIfOdd" onClick={ () => (count % 2 !== 0) ? update('INCREMENT') : null }>Increment if odd</button>
          <button id="incrementAsync"onClick={ () => setTimeout(() => update('INCREMENT'), 1000)}>Increment async</button>
        </p>
      </div>
    )
  }
}

function mapStateToProps(state, props) {
  return {
    count: state,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    update: (val) => {
      dispatch({type: val})
    }
  }
}

const CounterContainer = connect(mapStateToProps, mapDispatchToProps)(Counter);


class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <CounterContainer />
      </div>
    );
  }
}

export default App;
